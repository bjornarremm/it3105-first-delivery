import random

__author__ = 'sniken'
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ConStrN import ConStrN

from CheckGrid import *
from CheckGrid import CheckGrid
from copy import deepcopy
class SearchNode():
    colors = ["green","red"]
    defaultcolor = 'grey'
    endState = None
    onNodeupdate = None


    def __init__(self,pos,parent):
        self.parent = parent  # points to best parentnode- id?
        self.State = pos # A state from

        self.Grid =[[0 for x in xrange(CheckGrid.height)] for x in xrange(CheckGrid.width)]
        self.fromDomainToGrid()  # Saving states inside Grid

        self.kids = None  # Kid in form of
        self.g =parent.g if parent is not None else 0
        self.f = self.fval()   # Fvalue to be able




    def fromDomainToGrid(self):
        for key,ValueOfDomain in self.State.domains.items():
            for gridcells in ValueOfDomain:
                if key[0] == 'c':
                    x = int(key[1:])
                    for y in xrange(CheckGrid.height):
                        self.Grid[x][y] =gridcells.bitSequence[y]

                elif key[0] == 'r':
                    y = int(key[1:])
                    for x in xrange(CheckGrid.width):
                        self.Grid[x][y] =gridcells.bitSequence[x]

                else:
                    print "Something wrong in GRID"

        self.Grid = [[[int(cell) for cell in row] for row in col] for col in self.Grid]


    def __str__(self):
        return str(self.State)

    def heuristic(self):                # H = multiplying domain length with each other.
        return self.State.domainLength()

    def generatehash(self):
        return str(self.State.domains)

    def fval(self):                     # Returning fval. The cost is not important.
        return self.heuristic()

    def numberofVariablesinDomain(self):
        return sum([len(ar) for ar in self.State.domains.values()])

    def isFinished(self):
        SearchNode.onNodeupdate(self)       # GUI in main. Draws red / grey
        return len(self.State.domains.keys()) == self.numberofVariablesinDomain()   # should return true if 1



    def shortestInDomain(self):
        shortestDomainLength = 99999999 # infinity
        min_key = None

        for var in self.State.domains:
            domain = self.State.domains[var]
            DomainLength = len(domain)
            if DomainLength < shortestDomainLength and DomainLength != 1:
                shortestDomainLength = DomainLength
                min_key = var

        return min_key   # finds the shortest domain and returns the key

    def genereateChildren(self):
        if self.kids is None:  # if not generated before
            self.kids =[]
            shortestInDomain =self.shortestInDomain()

            if shortestInDomain != None:
                domain = self.State.domains[shortestInDomain]
                for value in domain:
                    dpcopy = deepcopy(self.State.domains)       # deepcopy of domain
                    dpcopy[shortestInDomain] = [value]          # makes a guess for every child
                    SN = SearchNode(ConStrN(dpcopy),self)       # New SearchNode with domain.
                    SN.State.rerunQueue(shortestInDomain)       # reruns queue
                    #print "Hei"
                    if(SN.State.ac3()):                         # if added to queue this will run
                         self.kids.append(SN)
        return self.kids
