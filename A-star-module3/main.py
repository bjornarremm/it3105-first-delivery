

from Constraint import Constraint
from ConStrN import  ConStrN
from CheckGrid import *
from SearchNode import SearchNode
from Astar import astar
from GUI import App

import sys
from copy import deepcopy
def main():
    a = CheckGrid(sys.argv[1])
    cn = ConStrN(a.alldict)

    ConStrN.constraints = a.ConstraintsList
    cn.InitilizeQueue()

    #gui = App(CheckGrid.height,CheckGrid.width)

    gui = App(CheckGrid.width, CheckGrid.height)



    sn = SearchNode(cn,None)

    def setRowColor(SN):
        gui.clearGrid(CheckGrid.height,CheckGrid.width)
        #gui.clearGrid(CheckGrid.height,CheckGrid.width)
        for x in xrange(CheckGrid.width):
            for y in xrange(CheckGrid.height):
                if SN.Grid[x][y][0] ==1:
                    gui.drawrect(0,x,y,'red')
                elif SN.Grid[x][y][0] != 1:
                    gui.drawrect(0,x,y,'grey')





    SearchNode.onNodeupdate = setRowColor


    def drawPath(path,generatednodes):
        print "Lengde av stien: ",len(path)
        print "Genrerte noder: ", generatednodes

    ast = astar(sn)
    ast.pathDoneUpdate = drawPath
    print "Antall noder i treet: " ,(int(a.width) * int(a.height))

    astar.popmethod = 'astar'                                # astar , bfs, dfs
    p = ast.best_first_search(sn)

    gui.mainloop()

if __name__ == "__main__": main()
app = App()
app.mainloop()