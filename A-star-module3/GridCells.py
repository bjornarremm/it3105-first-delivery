__author__ = 'Bjornars'
class GridCells:
    def __init__(self, bitNum, bitSequence):
        self.bitNum, self.bitSequence = bitNum, bitSequence


    def CheckifLegal(self, col):
        return self.bitSequence[col.bitNum] == col.bitSequence[self.bitNum]
