__author__ = 'sniken'

import itertools
import re


from Constraint import Constraint
from GridCells import GridCells

class CheckGrid:
    height = None
    width = None
    def __init__(self,filename):
        f = open("files/"+filename, "r").read()     # run writing python main.py filename
        f = [line for line in f.split("\n")]
        CheckGrid.width,CheckGrid.height = f[0].split(" ")
        CheckGrid.height = int(CheckGrid.height)
        CheckGrid.width = int(CheckGrid.width)

        rows = f[1:int(CheckGrid.width)+1]

        coloumns = f[int(CheckGrid.height)+1:]

        self.rowsDict= {}
        self.colsDict ={}

        for index,line in enumerate(rows):                                                  # making dict with rows {r1: [3,1,3]
            self.rowsDict["r"+str(index)] = line.split(" ")
            self.rowsDict["r"+str(index)] = [int(i) for i in self.rowsDict["r"+str(index)]]
        print "Length of rowdict: ", len(self.rowsDict)

        for index,line in enumerate(coloumns):                                              # making dict with coloumns {c1: [3,1,3]
            self.colsDict["c"+str(index)] = line.split(" ")
            self.colsDict["c"+str(index)] = [int(i) for i in self.colsDict["c"+str(index)]]
        print "Length of colsDict: ", len(self.colsDict)



        self.alldict = {key: list(self.genBits(ConsTable, CheckGrid.width)) for key, ConsTable in self.rowsDict.items()}        # ConsTable = The constraints of the row (3,1,3)
        self.alldict.update({key: list(self.genBits(ConsTable, CheckGrid.height)) for key, ConsTable in self.colsDict.items()}) # ConsTable = The constraints of the row (3,1,3)
        self.alldict = {key: [GridCells(int(key[1:]), cellData) for cellData in dat] for key, dat in self.alldict.items()}

        self.ConstraintsList=[]
        for row in self.rowsDict.keys():
            for col in self.colsDict.keys():
                c =Constraint([row, col], row+".CheckifLegal(" + col + ")")
                self.ConstraintsList.append(c)

    def genBits(self,pattern, length):
        regex = "0*" + '0+'.join(["1{" + str(i) + "}" for i in pattern]) + "0*"
        reg = re.compile(regex)

        result = []
        for bits in itertools.combinations(xrange(length), sum(pattern)):
            s = ['0'] * length
            for bit in bits:
                s[bit] = '1'
            result.append(''.join(s))
        return [i for i in result if reg.match(i)]


