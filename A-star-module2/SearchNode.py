import random

__author__ = 'sniken'
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ConStrN import ConStrN
from copy import deepcopy
import time
class SearchNode():
    colors = ["green", "blue", "red", 'orange', 'yellow','black','purple','brown']  # Specifix for this solution
    defaultcolor = 'grey'
    endState = None
    onNodeupdate = None

    def __init__(self,pos,parent):
        self.parent = parent  # points to best parentnode- id?
        self.State = pos
        self.kids = None  # Kid in form of
        self.g =parent.g if parent is not None else 0
        self.f = self.fval()   # Fvalue to be able
        self.cost = 1



    def __str__(self):
        return str(self.State)

    def heuristic(self):
        return self.State.domainLength()

    def generatehash(self):
        return str(self.State.domains)

    def fval(self):
        return self.heuristic()

    def numberofVariablesinDomain(self):
        return sum([len(ar) for ar in self.State.domains.values()])

    def isFinished(self):
        #if SearchNode.onNodeupdate:
        for key,values in self.State.domains.iteritems():
            color = SearchNode.defaultcolor if len(values) !=1 else SearchNode.colors[values[0]]
            SearchNode.onNodeupdate(color,key)
            #time.sleep(0.5)
        return len(self.State.domains.keys()) == self.numberofVariablesinDomain()

    def shortestInDomain(self):
        shortestDomainLength = 99999999 # infinity
        shortestDomainVar = None

        for var in self.State.domains:
            domain = self.State.domains[var]
            DomainLength = len(domain)
            if DomainLength < shortestDomainLength and DomainLength != 1:
                shortestDomainLength = DomainLength
                shortestDomainVar = var

        return shortestDomainVar


    def genereateChildren(self):
        if self.kids is None:  # if not generated before
            self.kids =[]

            shortestDomainVar =self.shortestInDomain()
            #print "shortestdomainvar", shortestDomainVar

            #print "shortestDomain"
            #print shortestDomainVar
            if shortestDomainVar != None:
                domain = self.State.domains[shortestDomainVar]
                #print "domain ",domain
                for value in domain:
                    dpcopy = deepcopy(self.State.domains)
                    dpcopy[shortestDomainVar] = [value]
                    SN = SearchNode(ConStrN(dpcopy),self)
                    SN.State.rerunQueue(shortestDomainVar)
                    #print "Hei"
                    if(SN.State.ac3()):
                         self.kids.append(SN)
        return self.kids









