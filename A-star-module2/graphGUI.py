from Tkinter import Tk, Canvas
from threading import Thread

class GraphUI(Canvas):
    def __init__(self, parent, graph):
        Canvas.__init__(self, parent, width=graph.width, height=graph.height)

        for e in graph.edges:
            self.create_line(*[i for j in e for i in graph.vertex[j]])

        self.vertex = {k: self.createCircle(v[0], v[1], 6, fill="black") for k, v in graph.vertex.items()}
        self.pack()


    def createCircle(self, x, y, r, **kwargs):
        return self.create_oval(x-r, y-r, x+r, y+r, **kwargs)


    def setVertexColor(self, color, id):
        self.itemconfig(self.vertex[id], fill=color)


def getNewGraphWindow(graph):
    root = Tk()
    root.title("2D Graph")
    app = GraphUI(root, graph)

    mainThread = Thread(target=root.mainloop)
    mainThread.start()
    return app