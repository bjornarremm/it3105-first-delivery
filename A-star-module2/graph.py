from operator import itemgetter

class Graph():
    def __init__(self, filename, height=800):
        f = open(filename, "r")

        self.NV, self.NE = [int(i) for i in f.readline().strip().split(" ")]

        vertexTemp = [[float(i) for i in f.readline().strip().split(" ")][1:] for i in xrange(self.NV)]
        xOffset, yOffset = min(vertexTemp, key=itemgetter(0))[0], min(vertexTemp, key=itemgetter(1))[1]
        xScope = max(vertexTemp, key=itemgetter(0))[0] - xOffset
        yScope = max(vertexTemp, key=itemgetter(1))[1] - yOffset

        self.height, self.width = height, xScope * height/yScope
        yScaling, xScaling = (self.height-20.0)/yScope, (self.width-20.0)/xScope

        self.vertex = {"n" + str(j): (int((i[0]-xOffset)*xScaling + 10), int((i[1]-yOffset)*yScaling + 10)) for i, j in zip(vertexTemp, range(len(vertexTemp)))}

        self.edges = [["n" + str(j) for j in f.readline().strip().split(" ")] for i in xrange(self.NE)]
