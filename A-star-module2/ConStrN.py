__author__ = 'sniken'

# Constraint network

# A CSP consists of 3 main components: variables, their domains, and constraints.

import itertools


class ConStrN:

    def __init__(self, domains):

        self.domains = domains
        self.consQueue = []

    def domainLength(self):
        tempLength=1
        for obj in self.domains.values():
            tempLength*=len(obj)
        return tempLength

    def InitilizeQueue(self):   # Initialize the queue
        #print ConStrN.constraints[0]
        for cons in ConStrN.constraints:
            for var in cons.variables:   # Appending variables together with constraints
                self.consQueue.append((var,cons))# first var in constraint. Second is constraint.
                #print self.consQueue[0][1]


    def domainfilterloop(self,variable,constraint): #filters until the queue is empty. Then Astar reruns the algorithm
        for cons in ConStrN.constraints:
            if cons == constraint: continue # if constraint and variable match up.
            for var in cons.variables: # find the variable in that type of constraint
                if var == variable: continue
                self.consQueue.append((var,cons))

    def rerunQueue(self,variable): #given the assumption V . uses Constraints of variable
        for cons in ConStrN.constraints:
            if variable not in cons.variables: continue
            for var in cons.variables:
                if var == variable: continue
                self.consQueue.append((var,cons)) # saved as tuple ( variable , constraint)


    def ac3(self):
        #self.InitilizeQueue() # Iniitilize Queue
        #print self.consQueue
        while self.consQueue: # While constraintsQueue not empty
            var,cons = self.consQueue.pop(0)  # Pop variable and constraint from Queue. tuple!

            if self.arc_reduce(var,cons): #
                if not self.domains[var]:
                    return False
                self.domainfilterloop(var, cons)  # If not False.
        return True

    def isSatisfiable(self,domain,Constraint):
        for comb in itertools.product(*domain): # *var means - collects all the positional arguments in a tuple
            if(apply(Constraint.function,comb)):
                return True
        return False


    def arc_reduce(self,Variable,Constraint):
        change = False
        for XkValue in self.domains[Variable]:  # Loops  over index over domain for Xkvalue
            args =[]

            for variable in Constraint.variables:  # loops over all the variables in the constraintNetwork
                if variable == Variable:         # if the variable matches one in constraintNetwork
                    args.append([XkValue])  # adds variable(s)
                else:
                    args.append(self.domains[variable])

            if not self.isSatisfiable(args,Constraint):  # if XkValues is false - There is no possibilites of other domains working with XkValue
                self.domains[Variable].remove(XkValue)  # The XkValue is removed from the list in the domain.
                change = True
        return change







        '''  Next, an assumption is made that node C is colored red. This causes all variables related to C by aconstraint to be paired with that constraint in a TODO REVISE req
        '''
        #After choosing color F from x
        # remove color F from adjacent.

    #Three such requests are placed
    #on the queue, and REVISE* gets called three times, with each call resulting in red being removed from the
    #domain of variables A, D and B.

