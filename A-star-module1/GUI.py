import Tkinter as tk
import Astar
from tkFileDialog import askopenfilename
import random


class App(tk.Tk):
    cellsize = 40
    globalW, globalH = 10, 10

    canvas = None



    def __init__(self, rows, coloumns):
        tk.Tk.__init__(self)
        self.canvas = tk.Canvas(self, width=rows*App.cellsize, height=coloumns*App.cellsize, borderwidth=0, highlightthickness=0)
        self.canvas.pack(side="top", fill="both", expand="true")

        self.rect = {}
        #self.rect = {}
        for column in range(coloumns):
            for row in range(rows):
                x1 = column*App.cellsize
                y1 = row * App.cellsize
                x2 = x1 + App.cellsize
                y2 = y1 + App.cellsize
                self.rect[column,rows-1-row] = self.canvas.create_rectangle(x1,y1,x2,y2, fill="WHITE", tags="rect")



    def drawrect(self, delay, row, col,colour):
        #self.canvas.itemconfig("rect", fill="blue")
        #self.canvas.itemconfig("rect", fill="blue")
        #for i in range(100):
           #row = random.randint(0,99)
           #col = random.randint(0,99)
        item_id = self.rect[row,col]

        self.canvas.itemconfig(item_id, fill=colour)

    def clearCanvas(self,size, walls):
        for col in range (size[0]):
            for row in range(size[1]):
                item_id = self.rect[row,col]
                tuple = (row,col)
                if tuple not in walls:
                    self.canvas.itemconfig(item_id,fill="white")

    def drawfinishedpath(self,retracepath):
        for tile in retracepath:
            tile_id = self.rect[tile.State[0],tile.State[1]]
            self.canvas.itemconfig(tile_id,fill="red")
            #self.after(1000, lambda: self.drawopen(1000))

    def draw_barriers(self,barriers):
       # print barriers
        for tile in barriers:
            tile_id = self.rect[tile[0],tile[1]]
            self.canvas.itemconfig(tile_id, fill="grey")

    def drawbestline(self,current):
        currentparent = current   # looping through found path
        finalpath = []
        while currentparent.parent:
            finalpath.insert(0,currentparent)
            currentparent = currentparent.parent
            tile_id = self.rect[currentparent.State[0],currentparent.State[1]]
            self.canvas.itemconfig(tile_id,fill="red")



    def drawArr(self,arr,color):
        for tile in arr:
            tile_id = self.rect[tile.State[0],tile.State[1]]
            self.canvas.itemconfig(tile_id,fill=color)




    def drawcurrent(self,current):
        item_id = self.rect[current.State[0],current.State[1]]
        self.canvas.itemconfig(item_id, fill="pink")
        #self.after(1000,lambda: self.drawcurrent(1000))








        #self.intitialize(10,10)



if __name__ == "__main__":
    app = App()
    app.mainloop()

