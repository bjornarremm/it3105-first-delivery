__author__ = 'Bjornars'


import Astar
from CheckGrid import CheckGrid
from SearchNode import SearchNode
from GUI import App
import sys



def main():

    data = open("files/"+sys.argv[2], "r").read() # reads in file
    # #use either bfs / astar or dfs. Like this inside cmd python main.py astar 0
    # Where the last one is the name of the file

    data = [line for line in data.split('\n') if line.strip() != '']    # checks if there is spaces or similar
    #size = eval(data[0]) # Sets size
    size0,size1 = data[0].split(" ")
    #data[1] = data[1].replace(" ","")                                   # removes spaces
    startend = data[1].split(" ")                                      # splits on second line
    startnode = (int(startend[0]),int(startend[1]))                                 # makes first node
    SearchNode.endState = (int(startend[2]),int(startend[3]))                         # makes second node
    startSearch = SearchNode(startnode,None)
    Astar.astar.popmethod = sys.argv[1]                                 # astar , bfs, dfs
    walls =[]
    for i in data[2:]:
        temp = i.split(" ")
        walls.append((int(temp[0]),int(temp[1]),int(temp[2]),int(temp[3])))
        # reads in obstacles

    CheckGrid.initiate(int(size0),int(size1),walls)                           # 10x10 matrix // size0 and 1 is the boundaries. ie 10x10 matrix
    gui = App(int(size0),int(size1))
    size = (int(size0),int(size1))
    gui.draw_barriers(CheckGrid.getwalls())


    def updateGUI(currentnode,closedlist,openlist):
        gui.clearCanvas(size, CheckGrid.getwalls())                     # clears canvas for colors if not wall
        gui.drawArr(list(closedlist),"yellow")                          # draws closedlist
        gui.drawArr(openlist,"black")                                   # draws openlist
        gui.drawbestline(currentnode)                                   # draws the current best line
        gui.update()
    def drawPath(path,generatednodes):
        gui.drawfinishedpath(path)
        gui.update()


    aStar = Astar.astar(startSearch,updateGUI,drawPath)
    aStar.best_first_search(startSearch)

    gui.mainloop()

if __name__ == "__main__": main()
app = App()
app.mainloop()
