import time

__author__ = 'sniken'
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from SearchNode import SearchNode
import CheckGrid
from Queue import PriorityQueue
import time

class astar():

    popmethod =None
    ExpandedNodes =0

    def __init__(self, start,updateGUI,drawPath):
        self.openlist = [] #set()# set()  # remember we need hashmap or something similar with hash
        self.closedlist =set()# set()
        self.createdDict ={}
        self.start = start
        self.onNodeupdate = updateGUI
        self.pathDoneUpdate = drawPath




    def propagatepathimprovement(self, parent):  #  updates all nodes with their best parent
        if parent.kids != None:
            for kid in parent.kids:                # iterates trough all the kids and if it is possible to update better
                if((parent).g +parent.cost) <(kid.g):   # parent it does so.
                    kid.setparent(parent)
                    self.propagatepathimprovement(kid)  #recursive method






        # result has been found, return the final traced path.
    def retracepath(self,current):
        currentparent = current   # looping through found path
        finalpath = []
        while currentparent.parent:
            finalpath.insert(0,currentparent)
            currentparent = currentparent.parent

        return finalpath

    def choosePop(self):   #  choose either bfs dfs astar
        if astar.popmethod == "bfs":
            #print [x.State for x in self.openlist]

            return self.openlist.pop(0)
        elif astar.popmethod == "dfs":
            return self.openlist.pop(-1)
        elif astar.popmethod == "astar":
            minimum = min(self.openlist ,key=lambda x: x.f)
            self.openlist.remove(minimum)
            return minimum

    def best_first_search(self, start):


        self.openlist.append(start) # add to openlist


        while self.openlist:
            #print [x.State for x in self.openlist]


            #time.sleep(0.05)
            current = self.choosePop()  # current is the node with the lowest f-value ( A-star). dfs=last in list bfs = first in list

            if self.onNodeupdate: self.onNodeupdate(current,self.closedlist,self.openlist)
            #if current node is the same as the goal node. We have found our path.
            #now retrace our path.
            if current.isFinished():
                path = self.retracepath(current)
                generatednodes = len(self.openlist) + len(self.closedlist)
                #print path
                if self.pathDoneUpdate: self.pathDoneUpdate(path, generatednodes)
                return path

            self.closedlist.add(current)  # adds current node to closed

            succecors = SearchNode.genereateChildren(current)  # generating children

            # All the generated children
            for child in succecors: #  iterate through the children

                if child.generatehash() in self.createdDict:  # if generated before
                    child = self.createdDict[child.generatehash()]
                    if (current.g + current.cost) < child.g:  # then found cheaper path to S
                        child.setparent(current)
                        if child in self.closedlist:
                            self.propagatepathimprovement(child)
                else:  # if not generated before
                    self.createdDict[child.generatehash()] = child
                    self.openlist.append(child)
                    astar.ExpandedNodes +=1













'''
http://www.briangrinstead.com/blog/astar-search-algorithm-in-javascript
http://www.raywenderlich.com/4946/introduction-to-a-pathfinding

http://web.mit.edu/eranki/www/tutorials/search/

http://www.redblobgames.com/pathfinding/a-star/implementation.html


http://codereview.stackexchange.com/questions/55259/optimization-of-astar-in-java
Hvorfor bruke ID. Hvordan identifisere med hashtable


'''






'''
Relevante linker:
http://www.idi.ntnu.no/emner/it3105/materials/astar-details.pdf
http://www.redblobgames.com/pathfinding/a-star/implementation.html
http://www.briangrinstead.com/blog/astar-search-algorithm-in-javascript
http://www.raywenderlich.com/4946/introduction-to-a-pathfinding
http://www.growingwiththeweb.com/2012/06/a-pathfinding-algorithm.html
http://codereview.stackexchange.com/questions/55259/optimization-of-astar-in-java
'''








