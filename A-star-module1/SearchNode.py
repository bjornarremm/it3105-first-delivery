import random

__author__ = 'sniken'
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import CheckGrid

class SearchNode():

    endState = None


    def __init__(self, pos,parent):
        self.parent = parent  # points to best parentnode- id?
        self.State = pos  # position (x,y) tuple
        self.kids = None  # Kid in form of
        self.g = parent.g +1 if parent is not None else 0
        self.f = self.fval()   # Fvalue to be able
        self.cost = 1

    def setparent(self,parent):
        self.g = parent.g +1 if parent is not None else 0
        self.parent = parent

    def __str__(self):
        return str(self.State)

    def heuristic(self,current, goal):
        self.h = abs(goal[0] - current[0]) + abs(goal[1] - current[1])
        return self.h
    # Choose state with smallest sum of lengths of domain.
    # log of length
    def fval(self):
        return self.g +self.heuristic(self.State,SearchNode.endState)


    def generatehash(self):
        return str(self.State)
    def isFinished(self):
        return self.State == SearchNode.endState

    def genereateChildren(self):

        #Guess on smallest domain from state. This will ensure that you are not generating millions of children and have a big queue
        if self.kids is None:  # if not generated before
            self.kids = [SearchNode(i, self) for i in CheckGrid.CheckGrid.neighbors(self.State)]  # generate children
        random.shuffle(self.kids)  # shuffle to make it random which kids you choose.
        return self.kids







