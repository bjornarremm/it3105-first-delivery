__author__ = 'sniken'


class CheckGrid:
    @staticmethod
    def initiate(x_direction, y_direction,walls):
        CheckGrid.x_direction = x_direction
        CheckGrid.y_direction = y_direction
        CheckGrid.walls = set()
        for i in walls:
            for x in range(i[0],i[0]+i[2]): #  (4, 0, 4, 16)  from 4 to 8
                for y in range(i[1],i[1]+i[3]): # from 0 to 16
                    CheckGrid.walls.add((x,y))

    @staticmethod
    def in_bounds(id):
        (x, y) = id
        return 0 <= x < CheckGrid.x_direction and 0 <= y < CheckGrid.y_direction
    @staticmethod
    def passable(id):
        return id not in CheckGrid.walls

    @staticmethod
    def neighbors(id):
        (x, y) = id
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        if (x + y) % 2 == 0: results.reverse() # aesthetics
        results = filter(CheckGrid.in_bounds, results)  # filters out if they are outside of walls.
        results = filter(CheckGrid.passable, results)  # filters if they are inside walls.
        return results
    @staticmethod
    def getwalls():
        return CheckGrid.walls
